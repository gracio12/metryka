package bd.io.services;



import bd.io.model.Wyksztalcenie;
import bd.io.repository.WyksztalcenieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WyksztalcenieServiceImpl{
    private WyksztalcenieRepository wyR;

    @Autowired
    public void setWyksztalcenieRepository(WyksztalcenieRepository wyR) {
        this.wyR = wyR;
    }

    public Iterable<Wyksztalcenie> listAllWy() {
        return wyR.findAll();
    }

    public Wyksztalcenie getWyById(Long id) {
        return wyR.findOne(id);
    }

    public Wyksztalcenie saveWy(Wyksztalcenie wy) {
        return wyR.save(wy);
    }

}

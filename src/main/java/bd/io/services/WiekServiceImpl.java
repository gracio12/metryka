package bd.io.services;


import bd.io.model.Wiek;
import bd.io.repository.WiekRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WiekServiceImpl{
    private WiekRepository wiekRepository;

    @Autowired
    public void setWiekRepository(WiekRepository wiekRepository) {
        this.wiekRepository = wiekRepository;
    }

    public Iterable<Wiek> listAllW() {
        return wiekRepository.findAll();
    }

    public Wiek getWById(Long id) {
        return wiekRepository.findOne(id);
    }

    public Wiek saveW(Wiek w) {
        return wiekRepository.save(w);
    }

}

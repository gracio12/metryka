package bd.io.services;



import bd.io.model.Zamieszkanie;
import bd.io.repository.ZamieszkanieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ZamieszkanieImpl {
    private ZamieszkanieRepository zamieszkanieRepository;

    @Autowired
    public void setZamieszkanieRepository(ZamieszkanieRepository zamieszkanieRepository) {
        this.zamieszkanieRepository = zamieszkanieRepository;
    }


    public Iterable<Zamieszkanie> listAllZ() {
        return zamieszkanieRepository.findAll();
    }


    public Zamieszkanie getZById(Long id) {
        return zamieszkanieRepository.findOne(id);
    }


    public Zamieszkanie saveZ(Zamieszkanie z) {
        return zamieszkanieRepository.save(z);
    }

}

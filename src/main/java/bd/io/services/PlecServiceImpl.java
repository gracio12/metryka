package bd.io.services;


import bd.io.model.Plec;
import bd.io.model.Wiek;
import bd.io.repository.PlecRepository;
import bd.io.repository.WiekRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlecServiceImpl{
    private PlecRepository wiekRepository;

    @Autowired
    public void setPlecRepository(PlecRepository wiekRepository) {
        this.wiekRepository = wiekRepository;
    }

    public Iterable<Plec> listAllP() {
        return wiekRepository.findAll();
    }

    public Plec getPById(Long id) {
        return wiekRepository.findOne(id);
    }

    public Plec saveP(Plec p) {
        return wiekRepository.save(p);
    }

}

package bd.io.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by OpartyOtaczki on 01.05.2017.
 */
@Entity
public class Plec {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String odp1;
    private String odp2;
    private String odp3;
    private String odp4;
    private String odp5;
    private String odp6;
    private String odp7;
    private String odp8;
    private String odp9;
    private String odp10;


    public Plec(){
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOdp1() {
        return odp1;
    }

    public void setOdp1(String odp1) {
        this.odp1 = odp1;
    }

    public String getOdp2() {
        return odp2;
    }

    public void setOdp2(String odp2) {
        this.odp2 = odp2;
    }

    public String getOdp3() {
        return odp3;
    }

    public void setOdp3(String odp3) {
        this.odp3 = odp3;
    }

    public String getOdp4() {
        return odp4;
    }

    public void setOdp4(String odp4) {
        this.odp4 = odp4;
    }

    public String getOdp5() {
        return odp5;
    }

    public void setOdp5(String odp5) {
        this.odp5 = odp5;
    }

    public String getOdp6() {
        return odp6;
    }

    public void setOdp6(String odp6) {
        this.odp6 = odp6;
    }

    public String getOdp7() {
        return odp7;
    }

    public void setOdp7(String odp7) {
        this.odp7 = odp7;
    }

    public String getOdp8() {
        return odp8;
    }

    public void setOdp8(String odp8) {
        this.odp8 = odp8;
    }

    public String getOdp9() {
        return odp9;
    }

    public void setOdp9(String odp9) {
        this.odp9 = odp9;
    }

    public String getOdp10() {
        return odp10;
    }

    public void setOdp10(String odp10) {
        this.odp10 = odp10;
    }
}

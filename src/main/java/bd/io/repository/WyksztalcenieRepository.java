package bd.io.repository;



import bd.io.model.Wyksztalcenie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WyksztalcenieRepository extends CrudRepository<Wyksztalcenie, Long> {
}

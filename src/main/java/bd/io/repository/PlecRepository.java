package bd.io.repository;


import bd.io.model.Plec;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlecRepository extends CrudRepository<Plec, Long> {
}

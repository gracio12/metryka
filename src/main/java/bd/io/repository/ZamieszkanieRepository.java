package bd.io.repository;


import bd.io.model.Zamieszkanie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZamieszkanieRepository extends CrudRepository<Zamieszkanie, Long> {
}

package bd.io.repository;


import bd.io.model.Wiek;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WiekRepository extends CrudRepository<Wiek, Long> {
}

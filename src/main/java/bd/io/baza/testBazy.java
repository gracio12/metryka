package bd.io.baza;

import bd.io.model.Plec;
import bd.io.model.Wiek;
import bd.io.model.Wyksztalcenie;
import bd.io.model.Zamieszkanie;
import bd.io.repository.PlecRepository;
import bd.io.repository.WiekRepository;
import bd.io.repository.WyksztalcenieRepository;
import bd.io.repository.ZamieszkanieRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


/**
 * Created by OpartyOtaczki on 07.05.2017.
 */
@Component
public class testBazy implements ApplicationListener<ContextRefreshedEvent> {

    private WiekRepository wiekRepository;
    private ZamieszkanieRepository zamieszkanieRepository;
    private WyksztalcenieRepository wyksztalcenieRepository;
    private PlecRepository plecRepository;
    private Logger log = Logger.getLogger(testBazy.class);

    @Autowired
    public void setAllRepository(WiekRepository wiekRepository, ZamieszkanieRepository zamieszkanieRepository,WyksztalcenieRepository wyksztalcenieRepository,PlecRepository plecRepository) {
        this.wiekRepository = wiekRepository;
        this.zamieszkanieRepository = zamieszkanieRepository;
        this.wyksztalcenieRepository = wyksztalcenieRepository;
        this.plecRepository = plecRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        Wiek cos = new Wiek();
        cos.setOdp1("Ssvgs gfdgfgv fdgv gdfgv dfaha");
        cos.setOdp2("fgfngnfn");
        cos.setOdp3("https://springfdsg dfg sdfgdfs hs");
        cos.setOdp4("235268845711068308");
        cos.setOdp5("235268845711068ohhllhlhlh");
        wiekRepository.save(cos);
        log.info("Saved cos - id:" + cos.getId());
        Wiek cos2 = new Wiek();
        cos2.setOdp1("Ssvgs gfdgfgv fdgv gdfgv dfaha");
        cos2.setOdp2("fgfngnfn");
        cos2.setOdp3("https://springfdsg dfg sdfgdfs hs");
        cos2.setOdp4("235268845711068308");
        cos2.setOdp5("235268845711068ohhllhlhlh");
        cos2.setOdp6("235268845711068ohhllhlhlh");
        cos2.setOdp7("235268845711068ohhllhlhlh");
        cos2.setOdp8("235268845711068ohhllhlhlh");
        cos2.setOdp9("235268845711068ohhllhlhlh");
        cos2.setOdp10("235268845711068ohhllhlhlh");
        wiekRepository.save(cos2);
        log.info("Saved cos - id:" + cos2.getId());
        Zamieszkanie cos3 = new Zamieszkanie();
        cos3.setOdp1("Ssvgs gfdgfgv fdgv gdfgv dfaha");
        cos3.setOdp2("fgfngnfn");
        cos3.setOdp3("https://springfdsg dfg sdfgdfs hs");
        cos3.setOdp4("235268845711068308");
        cos3.setOdp5("235268845711068ohhllhlhlh");
        zamieszkanieRepository.save(cos3);
        log.info("Saved cos - id:" + cos3.getId());
        Wyksztalcenie cos4 = new Wyksztalcenie();
        cos4.setOdp1("Ssvgs gfdgfgv fdgv gdfgv dfaha");
        cos4.setOdp2("fgfngnfn");
        cos4.setOdp3("https://springfdsg dfg sdfgdfs hs");
        cos4.setOdp4("235268845711068308");
        cos4.setOdp5("235268845711068ohhllhlhlh");
        wyksztalcenieRepository.save(cos4);
        log.info("Saved cos - id:" + cos4.getId());
        Plec cos5 = new Plec();
        cos5.setOdp1("Ssvgs gfdgfgv fdgv gdfgv dfaha");
        cos5.setOdp2("fgfngnfn");
        cos5.setOdp3("https://springfdsg dfg sdfgdfs hs");
        cos5.setOdp4("235268845711068308");
        cos5.setOdp5("235268845711068ohhllhlhlh");
        plecRepository.save(cos5);
        log.info("Saved cos - id:" + cos5.getId());
    }


}

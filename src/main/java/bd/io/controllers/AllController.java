package bd.io.controllers;


import bd.io.model.Plec;
import bd.io.model.Wiek;
import bd.io.model.Wyksztalcenie;
import bd.io.model.Zamieszkanie;
import bd.io.services.PlecServiceImpl;
import bd.io.services.WiekServiceImpl;
import bd.io.services.WyksztalcenieServiceImpl;
import bd.io.services.ZamieszkanieImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AllController {

    private WiekServiceImpl wiekService;
    private ZamieszkanieImpl zamieszkanieService;
    private WyksztalcenieServiceImpl wyksztalcenieService;
    private PlecServiceImpl plecService;

    @Autowired
    public void setService(WiekServiceImpl wiekService, ZamieszkanieImpl zamieszkanieService, WyksztalcenieServiceImpl wyksztalcenieService, PlecServiceImpl plecService) {
        this.wiekService = wiekService;
        this.zamieszkanieService = zamieszkanieService;
        this.wyksztalcenieService = wyksztalcenieService;
        this.plecService = plecService;
    }

    @GetMapping(value = "/metryka")
    public String list(Model model) {
        model.addAttribute("wieks", wiekService.listAllW());
        model.addAttribute("zamieszkaniee", zamieszkanieService.listAllZ());
        model.addAttribute("wyksztalceniee", wyksztalcenieService.listAllWy());
        model.addAttribute("plecc", plecService.listAllP());
        System.out.println("Returning rpoducts:");
        return "metryka";
    }

    @RequestMapping(value = "/addMet")
    public String Met(){
        return "addMet";
    }

    @RequestMapping("/wiek/edit/{id}")
    public String editW(@PathVariable Long id, Model model) {
        model.addAttribute("wiek", wiekService.getWById(id));
        return "wiekformedit";
    }

    @RequestMapping("/wiek/new")
    public String newWiek(Model model) {
        model.addAttribute("wiek", new Wiek());
        return "wiekform";
    }

    @PostMapping(value = "wiek")
    public String saveWiek(Wiek w) {
        wiekService.saveW(w);
        return "redirect:/addMet";
    }

    @RequestMapping("/zamieszkanie/edit/{id}")
    public String editZ(@PathVariable Long id, Model model) {
        model.addAttribute("zamieszkanie", zamieszkanieService.getZById(id));
        return "zamieszkanieformedit";
    }

    @RequestMapping("/zamieszkanie/new")
    public String newZ(Model model) {
        model.addAttribute("zamieszkanie", new Zamieszkanie());
        return "zamieszkanieform";
    }

    @PostMapping(value = "zamieszkanie")
    public String saveZ(Zamieszkanie z) {
        zamieszkanieService.saveZ(z);
        return "redirect:/addMet";
    }

    @RequestMapping("/wyksztalcenie/edit/{id}")
    public String editWy(@PathVariable Long id, Model model) {
        model.addAttribute("wyksztalcenie", wyksztalcenieService.getWyById(id));
        return "wyksztalcenieformedit";
    }

    @RequestMapping("/wyksztalcenie/new")
    public String newWy(Model model) {
        model.addAttribute("wyksztalcenie", new Wyksztalcenie());
        return "wyksztalcenieform";
    }

    @PostMapping(value = "wyksztalcenie")
    public String saveWy(Wyksztalcenie wy) {
        wyksztalcenieService.saveWy(wy);
        return "redirect:/addMet";
    }

    @RequestMapping("/plec/edit/{id}")
    public String editP(@PathVariable Long id, Model model) {
        model.addAttribute("plec", plecService.getPById(id));
        return "plecformedit";
    }

    @RequestMapping("/plec/new")
    public String newP(Model model) {
        model.addAttribute("plec", new Plec());
        return "plecform";
    }

    @PostMapping(value = "plec")
    public String saveP(Plec w) {
        plecService.saveP(w);
        return "redirect:/addMet";
    }
}

package bd.io;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MetrykaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MetrykaApplication.class, args);
	}
}
